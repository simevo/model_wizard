LIBPF™ Model Wizard
===================

The **LIBPF™ Model Wizard** is a **free** and **open-source**, **browser-based** tool that can be used [online](https://libpf.com/sdk/model_wizard/) or run locally.

The LIBPF™ Model Wizard allows the **model developer** to generate interactively the **C++ code** required for a **process model**:</p>

- the Interface (.h file)

- the Implementation (.cc file)

- the Driver (.cc file)

for a **process model**.

The generated C++ code will not work out of the box: a few bits are missing, mainly due to the limitation of the JSON format that does not support multi-line strings and escaping (see xml's CDATA). Consider it a starting point !

For more information on **LIBPF™** (the **LIB**rary for **P**rocess **F**lowsheeting), check the [homepage](https://libpf.com).

For the **source code** to the LIBPF™ Model Wizard itself, check the [gitlab repo](https://gitlab.com/simevo/model_wizard).

# How to use

See the blog posts:

- [Automatic generation of C++ code for process models – 1 of 3](https://libpf.com/posts/2015-08-12-automatic-generation-of-c-code-for-process-models-1-of-3/): command-line C++ code generation from the JSON representation of process models with the jinja2 template engine

- [Automatic generation of C++ code for process models – 2 of 3](https://libpf.com/posts/2015-08-14-automatic-generation-of-c-code-for-process-models-2-of-3/): interactive code generation with the LIBPF™ model wizard

- [Automatic generation of C++ code for process models – 3 of 3](https://libpf.com/posts/2015-08-31-automatic-generation-of-c-code-for-process-models-3-of-3/): an example of the **complete model development workflow** using the LIBPF™ model wizard.

and the tutorial videos:

[![](https://img.youtube.com/vi/GIQs8ezBlf4/0.jpg)](https://www.youtube.com/watch?v=GIQs8ezBlf4 "LIBPF™ Model Wizard tutorial")

[![](https://img.youtube.com/vi/5kqnnHhKOvk/0.jpg)](https://www.youtube.com/watch?v=5kqnnHhKOvk "Detailed LIBPF™ Model Wizard demo")

# Browser support

Tested on:

- Google Chrome

- Firefox

- Microsoft Edge

- Safari on macOS and iOS

You need to have cookies (because we use local storage) and avaScript (of course !) enabled.

Failure to enable cookies results in Firefox in the quite opaque error "_SecurityError: The operation is insecure_".

# License

The LIBPF™ Model Wizard (C) Copyright 2015-2023 [simevo s.r.l.](https://simevo.com).

**GPLv3 License**:

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

Contains code borrowed from:

- [Bootstrap](https://getbootstrap.com/), the most popular HTML, CSS, and JS framework for developing responsive, mobile first projects on the web;

- Jeremy Dorn's [JSON Schema Based Editor](https://github.com/jdorn/json-editor) with a [little fix](https://github.com/jdorn/json-editor/issues/432);

- Mozilla's [Nunjucks](https://mozilla.github.io/nunjucks/), a JavaScript implementation of the [jinja2 templating language](https://jinja.palletsprojects.com);

- [Viz.js](https://github.com/mdaines/viz.js), Mike Daines's port of [graphviz](https://graphviz.org/) to javascript, based on [emscripten](https://emscripten.org/);

- [highlight.js](https://highlightjs.org/), JavaScript syntax highlighting library.

# Local installation

Copy the following files and directories to a known location, for example `/tmp/model_wizard`:

- model_wizard.html

- *.json

- views/

- css/

- fonts/

- js/

then start a web server from this location, for example:

    cd /tmp/model_wizard
    python3 -m http.server

or:

    cd /tmp/model_wizard
    php -S 0:8000

finally point your browser to the location served by your web server, for example: [http://localhost:8000/model_wizard.html](http://localhost:8000/model_wizard.html).

# CLI usage

You can also generate the snippets from the command line via the provided `j2.py` Python script, for example:

    ./j2.py views/model_h.tmpl model_schema.json Orc.json
