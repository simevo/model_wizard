#!/usr/bin/env python3
# coding=utf-8

# ***** BEGIN LICENSE BLOCK *****
# This file is part of LIBPF
# (C) Copyright 2015 Paolo Greppi simevo s.r.l.
# ***** END LICENSE BLOCK ***** */

""" j2.py: generate cc / h file from JSON DSL """

import json
import sys

import jinja2
import jsonschema

if len(sys.argv) != 4:
    print("sample usage: %s views/model_h.tmpl model_schema.json Orc.json" % sys.argv[0])
    sys.exit(-1)

templateLoader = jinja2.FileSystemLoader(searchpath=".")
templateEnv = jinja2.Environment(loader=templateLoader)
template = templateEnv.get_template(sys.argv[1])

model_file = open(sys.argv[3], "r")
model_json = model_file.read()
model_file.close()
model = json.loads(model_json)

model_schema_file = open(sys.argv[2], "r")
model_schema_json = model_schema_file.read()
model_schema_file.close()
model_schema = json.loads(model_schema_json)
jsonschema.validate(model, model_schema)

print(template.render(model))
