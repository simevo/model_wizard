all:
	yarnpkg install
	mkdir -p js
	mkdir -p css/fonts
	cp node_modules/bootstrap/dist/css/bootstrap.min.css.map css/.
	cp node_modules/bootstrap/dist/css/bootstrap.min.css css/.
	cp node_modules/bootstrap/dist/js/bootstrap.min.js* js/.
	cp node_modules/@popperjs/core/dist/umd/popper-lite.min.js js/.
	cp node_modules/@popperjs/core/dist/umd/popper-lite.min.js.map js/.
	cp node_modules/@json-editor/json-editor/dist/jsoneditor.js js/.
	cp node_modules/@json-editor/json-editor/dist/jsoneditor.js.map js/.
	cp node_modules/cdn-release/build/highlight.min.js js/.
	cp node_modules/cdn-release/build/styles/default.min.css css/highlight-default.css
	cp node_modules/nunjucks/browser/nunjucks.min.js js/.
	cp node_modules/nunjucks/browser/nunjucks.min.js.map js/.
	cp node_modules/viz.js/lite.render.js js/.
	cp node_modules/viz.js/viz.js js/.
	cp node_modules/bootstrap-icons/font/bootstrap-icons.min.css css/.
	cp node_modules/bootstrap-icons/font/fonts/bootstrap-icons.woff css/fonts/.
	cp node_modules/bootstrap-icons/font/fonts/bootstrap-icons.woff2 css/fonts/.
